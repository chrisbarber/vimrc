nmap <space> <leader>
colo highlight
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab
set nowrap
let g:gitgutter_override_sign_column_highlight = 0
let g:gitgutter_signs = 0
syntax on
set ruler
autocmd! GUIEnter * set vb t_vb=
set nocompatible
set backspace=indent,eol,start
set smartcase
set ignorecase
set showcmd
set history=1000
set undofile
autocmd FileType make setlocal noexpandtab
"set updatetime=100
set formatprg=par\ -w75j
autocmd FileType python nnoremap <leader>y :0,$!yapf<Cr><C-o>
set matchpairs+=<:>
set breakindent

iabbrev ipdb import pdb; pdb.set_trace()

set t_RS=
set t_SH=

nnoremap <c-l> gt
nnoremap <c-h> gT

nnoremap <silent> <c-j> :call NextBlockInCol(0,0)<cr>
nnoremap <silent> <c-k> :call NextBlockInCol(1,0)<cr>
vnoremap <silent> <c-j> :<c-u>call NextBlockInCol(0,1)<cr>
vnoremap <silent> <c-k> :<c-u>call NextBlockInCol(1,1)<cr>
onoremap <silent> <c-j> :call NextBlockInCol(0,0)<cr>
onoremap <silent> <c-k> :call NextBlockInCol(1,0)<cr>
function! NextBlockInCol(backwards, visual) abort
    if a:visual
        normal! gv
    endif
    let flags = a:backwards ? 'b' : ''
    let col = virtcol('.')
    if search('\%(\S.*\zs.\|\zs\S\)\%' . (col+1) . 'v', 'Wzs' . flags)
        let next_blank = search('^\%(.*\S.*\%' . (col+1) . 'v\)\@!', 'Wnz' . flags)
        if next_blank > 0
            call setpos('.', [0, next_blank + (a:backwards ? 1 : -1), col, 0])
        else
            call search('\%(\%1l\|\%' . line('$') . 'l\).*\zs\%' . col . 'v', 'Wz' . flags)
        endif
    endif
endfunction


set path=.,,
runtime path.vim
function! SetGlobalPath(paths)
    let &path = a:paths
endfunction
call path#Set(function('SetGlobalPath'))

set wildmenu
set suffixes+=,.pyc
set wildignore=*.o,*.pyc,*.h5
nnoremap <leader>f :find *
nnoremap <leader>b :buffer *
nnoremap <leader>7 :set hlsearch!<cr>
augroup qf
    autocmd!
    autocmd QuickFixCmdPost l* lwindow
augroup END
set grepprg+=\ -Ro\ --include=\*.{py,js,json,html,yml,yaml,sh,ts,txt,in,tf,css,scss,cpp,hpp,c,h,tf,tfvars,rego}
command! -nargs=+ LGrep execute 'silent lgrep ' . <q-args> . ' .' | :redraw!
nnoremap <leader>g :keepjumps LGrep<Space>""<left>
nnoremap <leader>v :keepjumps silent lvimgrep<Space>// **<left><left><left><left>

nnoremap <c-p> :keepjumps lpr<enter>
nnoremap <c-n> :keepjumps lne<enter>


" modified from https://gist.github.com/romainl/7198a63faffdadd741e4ae81ae6dd9e6
function! Diff(spec)
    let cmd = "++edit #"
    if len(a:spec)
        let repo = fnamemodify(finddir('.git', '.;'), ':p:h:h')
        execute 'cd ' . l:repo
        let cmd = "!git -C " . shellescape(l:repo) . " show " . a:spec . ":" . expand("%")
        cd -
    endif
    vertical new
    setlocal bufhidden=wipe buftype=nofile nobuflisted noswapfile
    execute "read " . cmd
    silent 0d_
    diffthis
    wincmd p
    diffthis
endfunction
command! -nargs=? Diff call Diff(<q-args>)

function! NextHunk()
    Diff HEAD
    normal! ]c
    wincmd p
    quit
    diffoff
endfunction
function! PrevHunk()
    Diff HEAD
    normal! [c
    wincmd p
    quit
    diffoff
endfunction
nnoremap ]c :call NextHunk()<cr>
nnoremap [c :call PrevHunk()<cr>


nnoremap <leader>s :!git stash show -p stash@{}<left>


command! -bar -nargs=* Jump lexpr system('git jump --stdout ' . expand(<q-args>))
